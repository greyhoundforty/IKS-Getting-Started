# Creating a Multi-Zone Kubernetes Cluster
 
## Prerequisites
 - [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installed. This is our main command line interface for cluster operations. 
 - Proper [Infrastructure Permissions](https://console.bluemix.net/docs/containers/cs_users.html#infra_access) for creating a cluster.
 - [IBM Cloud CLI](https://console.bluemix.net/docs/cli/reference/bluemix_cli/get_started.html#getting-started) installed.
    - Kubernetes service plugin installed: `ibmcloud plugin install container-service -r Bluemix`
    - Container registry plugin installed `ibmcloud plugin install container-registry -r Bluemix`

## Part 1: Creating the Cluster

For this tutorial I will be creating a 12 node [Multizone cluster](https://console.bluemix.net/docs/containers/cs_clusters.html#multi_zone) in the US East region. My initial command will deploy a 4 node cluster in the WDC07 zone and then I'll add in the WDC04 and WDC06 zones for higher availability. 

You can find the `cli` creation options [here](https://console.bluemix.net/docs/containers/cs_clusters.html#clusters_cli). 

```shell
$ ibmcloud ks cluster-create --name rtk8s --kube-version 1.9.9 --zone wdc07 --machine-type u2c.2x4 --private-vlan 883 --public-vlan 849 --workers 4

Creating cluster...
OK

$ ibmcloud ks clusters | grep rtk8s
rtk8s           b7cbac16a5fe4c8e8786bf1530e7ae69   requested   19 seconds ago   4         us-east-wdc07     1.9.9_1520
```

Now we will add the other 2 zones for the us-east region. We will see our cluster grow from 4 to 12 worker nodes. 

```shell
$ ibmcloud cs zone-add --zone wdc06 --cluster rtk8s --worker-pools default --private-vlan 769 --public-vlan 767

$ ibmcloud ks zone-add --zone wdc04 --cluster rtk8s --worker-pools default --private-vlan 868 --public-vlan 868

$ ibmcloud ks clusters | grep rtk8s
rtk8s           b7cbac16a5fe4c8e8786bf1530e7ae69   deploying   3 minutes ago   12        Washington D.C.   1.9.9_1520
```

Unless you're spinning up an IKS cluster with Bare Metal Worker nodes the provisioning process should complete in 20-30 minutes in most cases. Once the clusters status changes from `deploying` to `normal` we can get started with our initial set up and configuration. 

## Part 2: Initial Configuration 
The first step to interact with your cluster is to grab the cluster configuration file using the command `ibmcloud ks cluster-config CLUSTER_NAME`. This will download the cluster configuration file and give you an `export` command to run. 

```
$ ibmcloud ks cluster-config rtk8s
```

#### Label Edge Worker Nodes

Edge worker nodes can improve the security of your Kubernetes cluster by allowing fewer worker nodes to be accessed externally and by isolating the networking workload in IBM Cloud Kubernetes Service. When these worker nodes are marked with the label `dedicated=edge`, other workloads cannot consume resources or interfere with networking. By default none of the worker nodes have the `edge` label. 

```
$ kubectl get nodes -l dedicated=edge
No resources found.
```

To label the nodes you would run the following command: `kubectl label nodes <NODE_IP> dedicated=edge`. Use the private IP address from the NAME column to identify the nodes. When using multi-zone clusters at least 2 edge worker nodes must be enabled in each zone for high availability of load balancer or Ingress pods.

```
$ kubectl label nodes 10.170.29.16 10.170.29.37 10.188.10.131 10.188.10.133 10.190.150.90 10.190.150.91 dedicated=edge
node/10.170.29.16 labeled
node/10.170.29.37 labeled
node/10.188.10.131 labeled
node/10.188.10.133 labeled
node/10.190.150.90 labeled
node/10.190.150.91 labeled

$ kubectl get nodes -l dedicated=edge
NAME            STATUS    ROLES     AGE       VERSION
10.170.29.16    Ready     <none>    1h        v1.9.9+IKS
10.170.29.37    Ready     <none>    1h        v1.9.9+IKS
10.188.10.131   Ready     <none>    1h        v1.9.9+IKS
10.188.10.133   Ready     <none>    1h        v1.9.9+IKS
10.190.150.90   Ready     <none>    1h        v1.9.9+IKS
10.190.150.91   Ready     <none>    1h        v1.9.9+IKS
```

After a worker node is labeled `dedicated=edge`, all subsequent Ingress and load balancers are deployed to that node. In order to properly redeploy your Application Load Balancer to the `edge` nodes you must run the following command: 

```shell
$ kubectl get services --all-namespaces -o jsonpath='{range .items[*]}kubectl get service -n {.metadata.namespace} {.metadata.name} -o yaml | kubectl apply -f - :{.spec.type},{end}' | tr "," "\n" | grep "LoadBalancer" | cut -d':' -f1
```

This will output some commands to run to redeploy your ingress controller and load balancer services. For multi-zone clusters you will see more than one command returned, you need to run them all. In my case the command output looked like this:

```shell
kubectl get service -n kube-system public-crb7cbac16a5fe4c8e8786bf1530e7ae69-alb1 -o yaml | kubectl apply -f -
kubectl get service -n kube-system public-crb7cbac16a5fe4c8e8786bf1530e7ae69-alb2 -o yaml | kubectl apply -f -
kubectl get service -n kube-system public-crb7cbac16a5fe4c8e8786bf1530e7ae69-alb3 -o yaml | kubectl apply -f -
```

Run all the commands in the output. You can check the progress by using the `describe` command:

```shell 
$ kubectl describe service -n kube-system public-crb7cbac16a5fe4c8e8786bf1530e7ae69-alb1
```

If you want to further restrict what can run on your edge worker nodes see [Preventing workloads from running on edge worker nodes](https://console.bluemix.net/docs/containers/cs_edge.html#edge_workloads).

#### Create Private Image Repository Namespace 

With the IBM Cloud Kubernetes offering you also get a private Image Registry. In order to start pushing images to the registry you first to login to the container registry service:

```shell
$ ibmcloud cr login 
```

You can now create your namespace using the command `ibmcloud cr namespace-add <namespace>`

```shell 
$ ibmcloud cr namespace-add thundercougarfalconbird
Adding namespace 'thundercougarfalconbird'...

Successfully added namespace 'thundercougarfalconbird'

OK
```

When we start pushing images in to the Image Registry we will use the namespace we just created. For instance if I was pushing my local hello-world image to the registry I would run the command: 

```shell 
$ docker push registry.ng.bluemix.net/thundercougarfalconbird/hello-world:1
```

#### Configure Cluster Logging

By default everything that is needed to use the IBM Cloud [Log Analysis](https://console.bluemix.net/catalog/services/log-analysis) service is installed on your cluster, it is just disabled until you create an initial configuration. In this example I will be telling the service to forward logs for the following parts of my cluster:

 - **container:** Information that is logged by a running container. Paths include anything written to STDOUT or STDERR of the container.
 - **ingress:** Information about the network traffic that comes into a cluster through the Ingress Application Load Balancer.
 - **worker:** Information that is specific to the infrastructure configuration that you have for your worker node. Worker logs are captured in syslog and contain               operating system events.
 - **kubernetes:** Information from the kubelet, the kube-proxy, and other Kubernetes events that happen in the worker node. that run in the kube-system namespace.
 - **kube-audit:** Information about cluster-related actions that is sent to the Kubernetes API server; including the time, the user, and the affected resource.

```shell 
$ ibmcloud cs logging-config-create rtk8s --logsource container,ingress,worker,kubernetes,kube-audit --type ibm --org "CDE TEAM" --space coolkids

Creating logging configuration for container logs in cluster rtk8s...
Setting the * namespace configuration because one was not specified.
Validating space and org names...
Creating logging configuration for ingress logs in cluster rtk8s...
(more output here ...)
```

You can find the full logging-config options [here](https://console.bluemix.net/docs/containers/cs_health.html#logging). 


# Part 2: Deploying Wordpress (Major Work in Progress)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Deploy Wordpress using a MySQL Container](#deploy-wordpress-using-a-mysql-container)
  - [Create MySQL Kubernetes secret](#create-mysql-kubernetes-secret)
  - [Create Wordpress and Mysql Persistent Volume Claims](#create-wordpress-and-mysql-persistent-volume-claims)
  - [Create Wordpress and MySQL deployments and services](#create-wordpress-and-mysql-deployments-and-services)
- [Deploy Wordpress using Compose for MySQL](#deploy-wordpress-using-compose-for-mysql)
  - [Create Compose for MySQL Service](#create-compose-for-mysql-service)
    - [Get Compose for MySQL connection details](#get-compose-for-mysql-connection-details)
  - [Create Wordpress Persistent Volume Claims](#create-wordpress-persistent-volume-claims)
  - [Create Wordpress Deployment and Service](#create-wordpress-deployment-and-service)
- [Deploy an Ingress for Wordpress](#deploy-an-ingress-for-wordpress)
  - [Non-TLS Ingress example](#non-tls-ingress-example)
  - [TLS Ingress Example](#tls-ingress-example)
- [Troubleshooting](#troubleshooting)
- [References](#references)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Deploy Wordpress using a MySQL Container

The first step is to clone this repository to work with the example deploymant files. 

```shell
git clone https://gitlab.com/greyhoundforty/IKS-Getting-Started.git
cd IKS-Getting-Started
```

### Create MySQL Kubernetes secret

Create a new file called password.txt in your current directory and put your desired MySQL password inside password.txt (Could be any string with ASCII characters).

```shell
echo "YOUR_PASSWORD" | tee password.txt
```

We need to make sure password.txt does not have any trailing newline. Use the following command to remove possible newlines.

```shell
tr -d '\n' <password.txt >.strippedpassword.txt && mv .strippedpassword.txt password.txt
```

We will now store this password as a [Kubernetes Secret](https://kubernetes.io/docs/concepts/configuration/secret/). Kubernetes secrets are encrypted by default which makes secrets a safer and a more flexible option to store sensitive data than to store this data verbatim in a pod definition or in a docker image.

```shell
$ kubectl create secret generic mysql-pass --from-file=password.txt
```

### Create Wordpress and Mysql Persistent Volume Claims

```shell 
$ kubectl create -f Examples/Wordpress/wordpress-mysql-pvc.yaml
```

### Create Wordpress and MySQL deployments and services

```shell 
$ kubectl create -f Examples/Wordpress/wordpress-mysql-deployment.yaml
$ kubectl create -f Examples/Wordpress/mysql-deployment.yaml
```

Run the following command to see how the deployment is going:

```shell
$ kubectl get pods -l app=wordpress
```

This should return a list of pods from the kubernetes cluster.

```bash
NAME                             READY     STATUS             RESTARTS   AGE
wordpress-66fff7ddb5-9hcpd       1/1       Running            0          1m
wordpress-66fff7ddb5-b4zm2       1/1       Running            0          1m
wordpress-66fff7ddb5-kgbp5       1/1       Running            3          1m
wordpress-mysql-cf9449df-6s8b2   1/1       Running            0          4m
```

If everything is up and running as expected proceed to the [Deploy an Ingress](#deploy-an-ingress-for-wordpress) portion of this guide. 

## Deploy Wordpress using Compose for MySQL

The first step is to clone this repository to work with the example deploymant files. 

```shell
$ git clone https://gitlab.com/greyhoundforty/IKS-Getting-Started.git
$ cd IKS-Getting-Started
```

### Create Compose for MySQL Service

```shell
$ ibmcloud service create compose-for-mysql Standard <your service name>
```

#### Get Compose for MySQL connection details

Go to Service credentials and view your credentials. Your MySQL hostname, port, user, and password are under your credential uri and it should look like this (in my screenshot the password has been obscured, but it is the string just after the username).

![MySQL Connection Information](images/mysql_connection_info.png)

### Create Wordpress Persistent Volume Claims

```shell 
$ kubectl create -f Examples/Wordpress/wordpress-pvc.yaml
```

### Create Wordpress Deployment and Service

Modify your wordpress-deployment.yaml file, change `WORDPRESS_DB_HOST`'s value to your MySQL hostname and port (i.e. value: <hostname>:<port>), and `WORDPRESS_DB_PASSWORD`'s value to your MySQL password.

And the environment variables should look (something) like this:

```yaml
    spec:
      containers:
      - image: wordpress:4.7.3-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: sl-us-south-1-portal.19.dblayer.com:50883
        - name: WORDPRESS_DB_USER
          value: admin
        - name: WORDPRESS_DB_PASSWORD
          value: xxxxxxxxxxxxxxxxxx
```

After you modified the wordpress-deployment.yaml, run the following command to create the WordPress deployment and service.

```shell
$ kubectl create -f Examples/Wordpress/wordpress-compose-deployment.yaml
```

When all your pods are running, run the following commands to check your pod names.

```shell
$ kubectl get pods -l app=wordpress
```

This should return a list of pods from the kubernetes cluster.

```shell
NAME                             READY     STATUS    RESTARTS   AGE
wordpress-66fff7ddb5-9hcpd       1/1       Running   0          12h
wordpress-66fff7ddb5-b4zm2       1/1       Running   0          12h
wordpress-66fff7ddb5-kgbp5       1/1       Running   5          12h
```

If everything is up and running as expected proceed to the [Deploy an Ingress](#deploy-an-ingress-for-wordpress) portion of this guide. 

## Deploy an Ingress for Wordpress
We are going to expose our Wordpress site using an [Ingress controller](https://console.bluemix.net/docs/containers/cs_ingress.html#ingress). An Ingress controller is a Kubernetes service that balances network traffic workloads in your cluster by forwarding public or private requests to your apps. You can use Ingress to expose multiple app services to the public or to a private network by using a unique public or private route. Before you create the Wordpress Ingress you will need to update the appropriate yaml file and substitute your domain for `example.com`. 

In order to use a custom domain with the Wordpress site we need to create a CNAME recoed to point to our Ingress Subdomain. To retrieve the ingress subdomain run the following command:

```shell
$ ibmcloud ks cluster-get YOUR_CLUSTER_NAME
```

Once you have the Ingress subdomain create the needed CNAME record at your domain registar. For example I am using the domain `wp.imaginary.cloud` so at my registar I create a CNAME record to point `wp` to `singlezone-rtk8s.us-east.containers.appdomain.cloud`

![](images/wp_dns_record.png)

### Non-TLS Ingress example

Before deploying the Ingress the domain name needs to updated in the `Examples/Wordpress/nontls-ingress.yaml` file.  

```shell
$ sed -i 's|example.com|YOURDOMAIN.COM|' Examples/Wordpress/nontls-ingress.yaml
$ kubectl create -f Examples/Wordpress/nontls-ingress.yaml
```

**Note**: if you are running these commands on a Mac use the following sed command instead:

```shell
$ sed -i '' 's|example.com|YOURDOMAIN.COM|' Examples/Wordpress/nontls-ingress.yaml
```

Once the DNS record has propagated you can visit your domain and you should be presented with the Wordpress initial configuration page. 

![](images/wp_imaginary_install.png)

### TLS Ingress Example

In order to use TLS with the ingress controller you need to store your certificate details in Kubernetes as a secret. I have been using [certbot](https://certbot.eff.org/) to generate [Let's Encrypt](https://letsencrypt.org) certiticates. With certbot installed you can run the following command (substitute example.com with your actual domain name). 

```shell
$ sudo certbot --text --agree-tos --email "YOUR_EMAIL" -d example.com --manual --preferred-challenges dns --expand --renew-by-default  --manual-public-ip-logging-ok certonly
```

This will prompt you to create a `TXT` DNS record that Let's Encrypt will use to verify that you control the domain. Create the required record and after a few moments you should have some certificates generated. With that completed we can store our certificate as a secret and create our TLS ingress.  

```shell
$ kubectl create secret tls SECRET_NAME --key ~/path/to/privkey.pem  --cert ~/path/to/cert.pem
$ sed -i 's|tls_secret_name|SECRET_NAME|' Examples/Wordpress/tls-ingress.yaml
$ sed -i 's|example.com|YOURDOMAIN.COM|g' Examples/Wordpress/tls-ingress.yaml
$ kubectl create -f Examples/Wordpress/tls-ingress.yaml
```

**Note**: if you are running these commands on a Mac use the following sed command instead:

```shell
$ sed -i '' 's|example.com|YOURDOMAIN.COM|' Examples/Wordpress/tls-ingress.yaml
$ sed -i '' 's|tls_secret_name|SECRET_NAME|' Examples/Wordpress/tls-ingress.yaml
```

Once the DNS record has propagated you can visit your domain and you should be presented with the Wordpress initial configuration page.

![](images/wp_imaginary_install.png)

## Troubleshooting

If you accidentally created a password with newlines and you can not authorize your MySQL service, you can delete your current secret using

```bash
$ kubectl delete secret mysql-pass
```

If you want to delete your services, deployments, and persistent volume claim, you can run

```shell
$ kubectl delete deployment,service,pvc,ingress -l app=wordpress
```

If you want to delete your persistent volume, you can run the following commands

```bash
$ kubectl delete -f '*-pvc.yaml'
```

If WordPress is taking a long time, you can debug it by inspecting the logs
```shell
$ kubectl get pods -l app=wordpress
$ kubectl logs [wordpress pod name]
```

## References
- This WordPress example is based on Kubernetes's open source example [mysql-wordpress-pd](https://github.com/kubernetes/kubernetes/tree/master/examples/mysql-wordpress-pd).
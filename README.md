# IKS-Getting-Started

Getting started with the IBM Cloud Kubernetes Service. Other than the initial configuration, all other parts of this guide should be singlezone/multizone agnostic unless specifically called out. 

## Prerequisites
 - [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installed. This is our main command line interface for cluster operations. 
 - Proper [Infrastructure Permissions](https://console.bluemix.net/docs/containers/cs_users.html#infra_access) for creating a cluster.
 - [IBM Cloud CLI](https://console.bluemix.net/docs/cli/reference/bluemix_cli/get_started.html#getting-started) installed.
 - [IBM Cloud CLI Plugins](https://console.bluemix.net/docs/containers/cs_cli_install.html#cs_cli_install) installed. These are needed for interaction with the Kubernetes offering and the Image registry service.

## Initial Configuration
 - [Single Zone Part 1: Ordering and initial configuration](/Single-Zone-Part1.md)
 - [Multi Zone Part 1: Ordering and initial configuration](/Multi-Zone-Part1.md)

## Deploying Wordpress
- [Deploy Wordpress using a MySQL Container](Examples/Worpress/README.md/#deploy-wordpress-using-a-mysql-container)
  - [Create MySQL Kubernetes secret](Examples/Worpress/README.md/#create-mysql-kubernetes-secret)
  - [Create Wordpress and Mysql Persistent Volume Claims](Examples/Worpress/README.md/#create-wordpress-and-mysql-persistent-volume-claims)
  - [Create Wordpress and MySQL deployments and services](Examples/Worpress/README.md/#create-wordpress-and-mysql-deployments-and-services)
- [Deploy Wordpress using Compose MySQL](Examples/Worpress/README.md/#deploy-wordpress-using-compose-mysql)
  - [Create Wordpress Persistent Volume Claims](Examples/Worpress/README.md/#create-wordpress-persistent-volume-claims)
- [Deploy an Ingress for Wordpress](Examples/Worpress/README.md/#deploy-an-ingress-for-wordpress)
  - [Non-TLS Ingress example](Examples/Worpress/README.md/#non-tls-ingress-example)
  - [TLS Ingress Example](Examples/Worpress/README.md/#tls-ingress-example)

## Deploy Helm Charts

## Deploy Gitea 

## Deploy the Traefik Ingress Controller
 - [Automatic Certificates with Let's Encrypt](#)

